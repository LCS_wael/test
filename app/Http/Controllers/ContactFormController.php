<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Mail\ContactFormMail;

class ContactFormController extends Controller
{

    public function create(){
        return view('contact.create');
    }

    public function store(){
        $data = request()->validate([
            'name' => 'required|min:3',
            'email' => 'required|email',
            'message' => 'required',
        ]);

        Mail::to('wael28kacem@gmail.com')->send(new ContactFormMail($data));

        session()->flash('message','Thanks for your message. we\'ll be in touch.');
        return redirect('contact');
    }

}
