<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Company;

class AddCompanyCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'contact:company {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Adds a new company';



    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $company = Company::create([
            'name' => $this->argument('name'),
            'phone' => '22547635'
        ]);
        $this->info('added with success');
    }
}
