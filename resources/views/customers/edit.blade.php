@extends('layouts.app')

@section('title')
Edit Details for {{ $customer->name }}
 @endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <h1>Edit Details for {{ $customer->name }}</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <form action="{{ route('update',$customer->id ) }}" method="POST" class="pb-5" enctype="multipart/form-data">
                @method('PATCH')
                @include('customers.form')
                <br>
                <button type="submit" class="btn btn-primary">Save Customer</button>

            </form>
        </div>
    </div>
<hr>


@endsection
