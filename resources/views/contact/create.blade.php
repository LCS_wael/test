@extends('layouts.app')
@section('title', 'Contact Us')
@section('content')
    <h1>contact Us</h1>

    @if( ! session()->has('message'))
        <form action="/contact" method="POST">
            <label for="name">Name</label>
            <div class="form-group">
                <input type="text" name="name" value="{{ old('name') }}" class="form-control">
                <div>{{ $errors->first('name') }}</div>
            </div>
            <br>
            <label for="email">Email</label>
            <div class="form-group">
                <input type="text" name="email" value="{{ old('email')  }}" class="form-control">
                <div>{{ $errors->first('email') }}</div>
            </div>
            <label for="message">Message</label>
            <div class="form-group">
                <textarea name="message" id="message" cols="30" rows="10" class="form-control">{{ old('name') }}</textarea>
                <div>{{ $errors->first('message') }}</div>
            </div>
            @csrf
            <button type="submit" class="btn btn-primary">Send Message</button>
        </form>
    @endif
@endsection
