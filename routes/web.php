<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::view('/', 'home');

Route::get('contact', 'ContactFormController@create');
Route::post('contact', 'ContactFormController@store');

/*Route::get('contact', function () {
    return view('contact');
});*/

Route::view('/about', 'about');

Route::get('/customers', 'CustomersController@index')->name('home');
Route::get('/customers/create', 'CustomersController@create')->name('add');
Route::post('/customers', 'CustomersController@store');
Route::get('/customers/{customer}', 'CustomersController@show');
Route::get('/customers/{customer}/edit', 'CustomersController@edit')->name('edit');
Route::patch('/customers/{customer}', 'CustomersController@update')->name('update');
Route::delete('/customers/{customer}', 'CustomersController@destroy')->name('delete');
//Route::resource('customers', 'CustomerController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
